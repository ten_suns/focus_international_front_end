// 客服
import * as types from '../mutation-types'

export default {
  state: {
    companyContactWay:'',
    companyImages:'',
    companyName:'',
    servicerImages: "",
    servicerMobile:"",
    servicerName:"",
  },
  mutations: {
    updatecompanyContactWay (state, payload) {
      state.companyContactWay = payload.companyContactWay
    },
    updatecompanyImages (state, payload) {
        state.companyImages = payload.companyImages
      },
      updatecompanyName (state, payload) {
        state.companyName = payload.companyName
      },
      updateservicerImages (state, payload) {
        state.servicerImages = payload.servicerImages
      },
      updateservicerMobile (state, payload) {
        state.servicerMobile = payload.servicerMobile
      },
      updateservicerName (state, payload) {
        state.servicerName = payload.servicerName
      },
  }
}
